<?php
/**
 * @file
 * Enables modules and site configuration for Razoreye site installation.
 */


use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function razoreye_biz_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Add a placeholder as example that one can choose an arbitrary site name.
  $form['site_information']['site_name']['#attributes']['placeholder'] = t('My site');
  $form['#submit'][] = 'razoreye_biz_form_install_configure_submit';
}

/**
 * Implements hook_form_FORM_ID_alter() for search_block_form().
 *
 * Allows the profile to alter the search block form.
 */
function razoreye_biz_form_search_block_form_alter(&$form, FormStateInterface $form_state) {
  $form['keys']['#attributes']['placeholder'] = t('Search');
}



/**
 * Implements hook_menu_links_discovered_alter().
 */
function razoreye_biz_menu_links_discovered_alter(&$links) {
  // Force all links to show as expanded.
  foreach ($links as $id => $link) {
    $links[$id]['expanded'] = 1;
  }
}



/**
 * @param string $text
 * @return string Return machine name for text.
 */
function _razoreye_biz_clean_alias($text) {
  return preg_replace('/\-+/', '-', strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '', str_replace(' ', '-', $text))));
}


