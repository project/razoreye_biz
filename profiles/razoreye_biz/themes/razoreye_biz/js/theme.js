(function ($, Drupal) {

  'use strict';

    Drupal.behaviors.razoreye_biz = {
        attach: function(context, settings) {
            var menu = '.mm-panels';
            $(menu).find('ul').removeClass("menu navbar-nav nav dropdown-menu");
            $(menu).find('li').removeClass("dropdown expanded");
            $(menu).find('a .caret').remove().html();
            $(menu).find('a').removeAttr('data-target');
            $(menu).find('a').removeAttr('data-toggle');


            var menuDesktop = '.navigation';
            //$(menuDesktop).find('ul').removeClass("menu navbar-nav nav dropdown-menu");
           // $(menuDesktop).find('li').removeClass("dropdown expanded");
            $(menuDesktop).find('a .caret').remove().html();
            $(menuDesktop).find('a').removeAttr('data-target');
            $(menuDesktop).find('a').removeAttr('data-toggle');

        }
  };
})(jQuery, Drupal);
