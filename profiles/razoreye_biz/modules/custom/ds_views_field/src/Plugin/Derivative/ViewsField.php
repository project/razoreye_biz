<?php

namespace Drupal\ds_views_field\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\views\Entity\View;

/**
 * A views field derivative definition.
 */
class ViewsField extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    foreach ($this->getViews() as $entity_type_id => $views) {
      foreach ($views as $view_info) {
        list($view_id, $display_id) = $view_info;
        if (!$view_storage = View::load($view_id)) {
          drupal_set_message(t('View %view_id does not exist', ['%view_id' => $view_id]), 'error');
          continue;
        }

        $view = $view_storage->getExecutable();
        $view->setDisplay($display_id);
        $key = "$entity_type_id:$view_id";

        // Just keep adding 1s, the key should have used the display initially
        // but we didn't and now we don't want ot have to update all the display
        // configuration.
        while (isset($this->derivatives[$key])) {
          $key = $key . '_1';
        }

        $this->derivatives[$key] = $base_plugin_definition;
        $this->derivatives[$key] += [
          'provider' => 'ds_views_field',
          'title' => $view->getTitle(),
          'entity_type' => $entity_type_id,
          'view_id' => $view_id,
          'display_id' => $display_id,
        ];
      }
    }

    return $this->derivatives;
  }

  /**
   * Gets the views to exposed a Ds fields.
   *
   * Format is:
   *
   * $entity_type_id = [
   *   [$view_name, $view_display],
   * ],
   *
   * @return array
   *   An array of views keyed by entity type.
   */
  protected function getViews() {
    return [
      'node' => [
        ['noticeboard', 'block_1'],
      ],
    ];
  }

}
