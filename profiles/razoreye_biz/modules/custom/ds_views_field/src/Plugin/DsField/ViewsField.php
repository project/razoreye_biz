<?php

namespace Drupal\ds_views_field\Plugin\DsField;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a generic bundle field.
 *
 * @DsField(
 *   id = "ds_views_field",
 *   deriver = "Drupal\ds_views_field\Plugin\Derivative\ViewsField"
 * )
 */
class ViewsField extends DsFieldBase {

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $viewStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, EntityStorageInterface $viewStorage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->viewStorage = $viewStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('view')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $configuration = $this->getConfiguration();

    $field_config = $this->getFieldConfiguration();
    $view_id = $field_config['view_id'];
    $display_id = $field_config['display_id'];

    return $this->renderView($view_id, $display_id, $configuration['hide_on_empty']);
  }

  /**
   * Render the view of topic pages.
   *
   * @param string $view_name
   *   The View Id.
   * @param string $display_id
   *   The display Id to render.
   * @param bool $hide_on_empty
   *   If TRUE it will hide the view otherwise, set a no results behaviour.
   *
   * @return array
   *   The renderable array that displays the view.
   */
  protected function renderView($view_name, $display_id, $hide_on_empty) {
    /** @var \Drupal\views\Entity\View $storage */
    $storage = $this->viewStorage->load($view_name);
    $view = $storage->getExecutable();
    $results = $view->preview($display_id);

    $build = [
      '#cache' => [
        'tags' => $view->getCacheTags(),
      ],
    ];

    if ($view->access($display_id) && (count($results['#rows']) || !$hide_on_empty)) {

      if ($title = $view->getTitle()) {
        $build['wrapper'] = [
          '#type' => 'container',
          '#attributes' => ['class' => [$view_name . '-wrapper']]
        ];
        $build['wrapper']['title'] = [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => $title,
        ];
      }

      $build['wrapper']['view'] = $results;
      return $build;
    }

    // We always require cache data otherwise it can cache an empty result.
    return BubbleableMetadata::createFromObject($view);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['hide_on_empty'] = [
      '#type' => 'checkbox',
      '#title' => 'Hide view if empty',
      '#default_value' => $config['hide_on_empty'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings) {
    $config = $this->getConfiguration();
    $summary[] = $config['hide_on_empty'] ? $this->t('Hidden if empty') : $this->t('Shown if empty');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    $configuration = [
      'hide_on_empty' => 0,
    ];

    return $configuration;
  }

}
