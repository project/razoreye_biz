<?php

namespace Drupal\website_information\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'MainEmailDetailsBlock' block.
 *
 * @Block(
 *  id = "main_email_details_block",
 *  admin_label = @Translation("Main email"),
 * )
 */
class MainEmailDetailsBlock extends BlockBase  {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('website_information.settings');
    $build = [];
    $build['config_main_email_details']  = $config->get('organisation_email');
    return $build;
  }

}
