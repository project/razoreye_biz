<?php

namespace Drupal\website_information\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ContactPhoneAddressEmailBlock' block.
 *
 * @Block(
 *  id = "contact_phone_address_email_block",
 *  admin_label = @Translation("Contact block(main phone, address, and email)"),
 * )
 */
class ContactPhoneAddressEmailBlock extends BlockBase  {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('website_information.settings');
    $build = [];
    $build['main_phone']  = $config->get('organisation_main_phone');
    $build['address']  = $config->get('organisation_address');
    $build['email']  = $config->get('organisation_email');
    return $build;
  }

}
