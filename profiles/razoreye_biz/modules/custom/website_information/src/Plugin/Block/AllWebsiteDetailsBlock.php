<?php

namespace Drupal\website_information\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'AllWebsiteDetailsBlock' block.
 *
 * @Block(
 *  id = "all_website_details_block",
 *  admin_label = @Translation("All website details"),
 * )
 */
class AllWebsiteDetailsBlock extends BlockBase{



  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('website_information.settings');
    $build = [];
    $build['name']        = $config->get('organisation_name');
    $build['address']     = $config->get('organisation_address');
    $build['main_phone']  = $config->get('organisation_main_phone');
    $build['email']       = $config->get('organisation_email');

    return $build;
  }

}
