<?php

namespace Drupal\website_information\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'MainPhoneDetailsBlock' block.
 *
 * @Block(
 *  id = "main_phone_details_block",
 *  admin_label = @Translation("Main phone number"),
 * )
 */
class MainPhoneDetailsBlock extends BlockBase  {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('website_information.settings');
    $build = [];
    $build['config_main_phone_details']  = $config->get('organisation_main_phone');
    return $build;
  }

}
