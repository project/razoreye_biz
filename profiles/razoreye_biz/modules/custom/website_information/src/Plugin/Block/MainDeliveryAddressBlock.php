<?php

namespace Drupal\website_information\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'MainDeliveryAddressBlock' block.
 *
 * @Block(
 *  id = "main_delivery_address_block",
 *  admin_label = @Translation("Main delivery address"),
 * )
 */
class MainDeliveryAddressBlock extends BlockBase  {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('website_information.settings');
    $build = [];
    $build['config_name']  = $config->get('organisation_name');
    $build['config_delivery_po_address']  = $config->get('organisation_delivery_po_address');
    $build['config_delivery_address']  = $config->get('organisation_delivery_address');
    $build['config_delivery_suburb']  = $config->get('organisation_delivery_suburb');
    $build['config_delivery_state']  = $config->get('organisation_delivery_state');
    $build['config_delivery_postcode']  = $config->get('organisation_delivery_postcode');
    return $build;


  }




}
