<?php
/**
 *
 */

namespace Drupal\website_information\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\KernelTests\Component\Utility\SafeMarkupKernelTest;

/**
 * Class WebsiteInformation.
 *
 * @package Drupal\website_information\Form
 */
class WebsiteInformationForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $configEditSettings;



  /**
   * WebsiteInformation constructor.
   * @param ConfigFactoryInterface $config_factory
   */
  function __construct(ConfigFactoryInterface $config_factory) {

    parent::__construct($config_factory);
    $this->configEditSettings =  \Drupal::service('config.factory')->getEditable('website_information.settings');

    //-- Setup save and delete objects;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'website_information.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'website_information_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = array(
      '#type' => 'fieldset',
      '#title' => t('Organization name'),
    );
    $form['name']['organisation_name'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value' => $this->configEditSettings->get('organisation_name'),
      '#attributes' => array(
        'placeholder' => t('Organisation name'),
      ),
    ];



    $form['address'] = array(
      '#type' => 'fieldset',
      '#title' => t('Address'),
    );
    $form['address']['organisation_address'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_address'),
      '#attributes' => array(
        'placeholder' => t('Organisation street address'),
      ),
    ];

    $form['address']['organisation_suburb'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_suburb'),
      '#attributes' => array(
        'placeholder' => t('Organisation suburb'),
      ),
    ];
    $form['address']['organisation_state'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_state'),
      '#attributes' => array(
        'placeholder' => t('Organisation state'),
      ),
    ];
    $form['address']['organisation_postcode'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_postcode'),
      '#attributes' => array(
        'placeholder' => t('Organisation postcode'),
      ),
    ];



    $form['delivery_address'] = array(
      '#type' => 'fieldset',
      '#title' => t('Delivery Address'),
    );

    $form['delivery_address']['organisation_delivery_po_address'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_delivery_po_address'),
      '#attributes' => array(
        'placeholder' => t('Organisation delivery post office address'),
      ),
    ];
    $form['delivery_address']['organisation_delivery_address'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_delivery_address'),
      '#attributes' => array(
        'placeholder' => t('Organisation delivery address'),
      ),
    ];
    $form['delivery_address']['organisation_delivery_suburb'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_delivery_suburb'),
      '#attributes' => array(
        'placeholder' => t('Organisation delivery suburb'),
      ),
    ];
    $form['delivery_address']['organisation_delivery_state'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_delivery_state'),
      '#attributes' => array(
        'placeholder' => t('Organisation delivery state'),
      ),
    ];
    $form['delivery_address']['organisation_delivery_postcode'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value'  => $this->configEditSettings->get('organisation_delivery_postcode'),
      '#attributes' => array(
        'placeholder' => t('Organisation delivery postcode'),
      ),
    ];




    $form['phones'] = array(
      '#type' => 'fieldset',
      '#title' => t('Phone numbers'),
    );
    $form['phones']['organisation_main_phone'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value' => $this->configEditSettings->get('organisation_main_phone'),
      '#attributes' => array(
        'placeholder' => t('Main organisation phone'),
      ),
    ];



    $form['online'] = array(
      '#type' => 'fieldset',
      '#title' => t('Online information'),
    );
    $form['online']['organisation_email'] = [
      '#title' => '',
      '#type' => 'textfield',
      '#default_value' => $this->configEditSettings->get('organisation_email'),
      '#attributes' => array(
        'placeholder' => t('Organisation email'),
      ),
    ];



    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => array('::newSubmissionHandlerSave'),
    ];


    return $form;

  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }


  /**
   * Custom submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function newSubmissionHandlerSave(array &$form, FormStateInterface $formState) {

  //  print  $formState->getValue('organisation_name');
  //  exit;
    if($formState->getValue('organisation_name') != '') {
      $this->configEditSettings
        ->set('organisation_name', $formState->getValue('organisation_name'))
        ->save();
    }


    //-- address
    if($formState->getValue('organisation_address') != '') {
      $this->configEditSettings
        ->set('organisation_address', $formState->getValue('organisation_address'))
        ->save();
    }

    if($formState->getValue('organisation_suburb') != '') {
      $this->configEditSettings
        ->set('organisation_suburb', $formState->getValue('organisation_suburb'))
        ->save();
    }



    if($formState->getValue('organisation_state') != '') {
      $this->configEditSettings
        ->set('organisation_state', $formState->getValue('organisation_state'))
        ->save();
    }


    if($formState->getValue('organisation_postcode') != '') {
      $this->configEditSettings
        ->set('organisation_postcode', $formState->getValue('organisation_postcode'))
        ->save();
    }


    //-- delivery
    if($formState->getValue('organisation_delivery_po_address') != '') {
      $this->configEditSettings
        ->set('organisation_delivery_po_address', $formState->getValue('organisation_delivery_po_address'))
        ->save();
    }

    if($formState->getValue('organisation_delivery_address') != '') {
      $this->configEditSettings
        ->set('organisation_delivery_address', $formState->getValue('organisation_delivery_address'))
        ->save();
    }

    if($formState->getValue('organisation_delivery_suburb') != '') {
      $this->configEditSettings
        ->set('organisation_delivery_suburb', $formState->getValue('organisation_delivery_suburb'))
        ->save();
    }


    if($formState->getValue('organisation_delivery_state') != '') {
      $this->configEditSettings
        ->set('organisation_delivery_state', $formState->getValue('organisation_delivery_state'))
        ->save();
    }

    if($formState->getValue('organisation_delivery_postcode') != '') {
      $this->configEditSettings
        ->set('organisation_delivery_postcode', $formState->getValue('organisation_delivery_postcode'))
        ->save();
    }


    //-- phone
    if($formState->getValue('organisation_main_phone') != '') {
      $this->configEditSettings
        ->set('organisation_main_phone', $formState->getValue('organisation_main_phone'))
        ->save();
    }

    //-- email
    if($formState->getValue('organisation_email') != '') {
      $this->configEditSettings
        ->set('organisation_email', $formState->getValue('organisation_email'))
        ->save();
    }
/*
    print $this->configEditSettings->get('organisation_name');
    print $this->configEditSettings->get('organisation_address');
    print $this->configEditSettings->get('organisation_phone');
    print $this->configEditSettings->get('organisation_email');
    exit;
//*/
    drupal_flush_all_caches();
  }



}
