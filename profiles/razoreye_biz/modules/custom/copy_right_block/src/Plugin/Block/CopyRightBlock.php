<?php

namespace Drupal\copy_right_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'CopyRightBlock' block.
 *
 * @Block(
 *  id = "copyright_block",
 *  admin_label = @Translation("Copyright Block"),
 * )
 */
class CopyRightBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var type
   */
  protected $formBuilder;

  /**
   *
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
  }


  /**
   *
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $output = '<div class="col-xs-12 col-md-6 p-none-left site-information-reserved">'
			. '<span class="site-reserved">&copy; All rights reserved by </span>'
			. '<span class="sitename">'
      . \Drupal::config('system.site')->get('name') . ' </span>'
			. '<span class="site-copy-right">&copy; Copyright </span>'
			. '<span class="site-date">' . date("Y") . '</span>'
			. '</div>';

    $build['copy_right_block']['#markup'] = $output;

    return $build;
  }

}
