<?php

namespace Drupal\our_link_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'OurLinkBlock' block.
 *
 * @Block(
 *  id = "our_link_block",
 *  admin_label = @Translation("Our Link Block"),
 * )
 */
class OurLinkBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var array
   */
  protected $formBuilder;

  /**
   *
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
  }


  /**
   *
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $output = '<div id="our-link-block" class="col-xs-12 col-md-6 p-none-right desktop-pull-right">'
      . '<a href="http://www.razoreye.com.au/" class="pull-right">Designed by Razoreye</a>'
      . '</div>';

    $build['copy_right_block']['#markup'] = $output;

    return $build;
  }

}
