<?php

namespace Drupal\ble_ds_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;

/**
 * Plugin that generates a title with event date.
 *
 * @DsField(
 *   id = "ble_ds_title_with_event_date",
 *   title = @Translation("Title with Event Date"),
 *   entity_type = "node"
 * )
 */
class TitleWithEventDate extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity = $this->entity();

    return [
      '#markup' => '<h2 class="underline event-title-with-date">' . $this->t('@title <span>• &nbsp;@date</span>', [
        '@title' => $entity->getTitle(),
        '@date' => (new \DateTime($entity->field_event_date->value))->format('d M Y'),
      ]) . '</h2>',
    ];
  }

}
