<?php

namespace Drupal\ds_map\Plugin\DsField;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\ds\Plugin\DsField\DsFieldBase;

/**
 * Plugin that renders the submitted by field.
 *
 * @DsField(
 *   id = "ds_map",
 *   title = @Translation("Map"),
 *   entity_type = "node",
 *   provider = "ds_map"
 * )
 */
class Map extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => new FormattableMarkup('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5471.441100747659!2d115.94378664820378!3d-32.10276312702099!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a3295d586872549%3A0x144aa901e0b3f04a!2sBletchley+Park+Primary+School!5e0!3m2!1sen!2sau!4v1495264542575" height="400" frameborder="0" style="width: 100%; border:0" allowfullscreen></iframe>', []),
    ];
  }

}
