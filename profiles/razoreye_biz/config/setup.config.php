<?php
$configArray = [
    'site_setup_form' => [
        'site_name' => 'Razoreye Biz',
        'site_mail' => 'business@business.com',
        'account_name' => 'admin',
        'account_mail' => 'admin@admin.com',
        'site_default_country' => 'AU',
        'date_default_timezone' => 'Australia/Melbourne',
        'update_notifications' => 0,
    ],
    'db_setup_form' => [
        'db' => 'beetbox',
        'db_user' => 'beetbox',
        'db_password' => 'beetbox'
    ]
];