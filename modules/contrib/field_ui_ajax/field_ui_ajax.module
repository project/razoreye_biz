<?php

/**
 * @file
 * Allows administrators to attach custom fields to fieldable types.
 */

use Drupal\field_ui_ajax\Component\Utility\HtmlExtra;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityViewModeInterface;
use Drupal\Core\Entity\EntityFormModeInterface;
use Drupal\Core\Url;
use Drupal\field_ui\FieldUI;
use Drupal\field_ui\Plugin\Derivative\FieldUiLocalTask;
use Drupal\field_ui_ajax\Controller\FieldUiAjaxController;

/**
 * Implements hook_entity_type_alter().
 */
function field_ui_ajax_entity_type_alter(array &$entity_types) {
  /** @var $entity_types \Drupal\Core\Entity\EntityTypeInterface[] */
  $entity_types['field_config']->setFormClass('edit', 'Drupal\field_ui_ajax\Form\FieldConfigAjaxEditForm');
  $entity_types['field_config']->setFormClass('delete', 'Drupal\field_ui_ajax\Form\FieldConfigAjaxDeleteForm');
  $entity_types['field_config']->setListBuilderClass('Drupal\field_ui_ajax\FieldConfigListBuilder');

  $entity_types['field_storage_config']->setFormClass('edit', 'Drupal\field_ui_ajax\Form\FieldStorageConfigAjaxEditForm');

  $entity_types['entity_form_display']->setFormClass('edit', 'Drupal\field_ui_ajax\Form\EntityFormDisplayAjaxEditForm');
  $entity_types['entity_view_display']->setFormClass('edit', 'Drupal\field_ui_ajax\Form\EntityViewDisplayAjaxEditForm');

  $form_mode = $entity_types['entity_form_mode'];
  $form_mode->setListBuilderClass('Drupal\field_ui_ajax\EntityFormModeAjaxListBuilder');
  $form_mode->setFormClass('add', 'Drupal\field_ui_ajax\Form\EntityFormModeAjaxAddForm');
  $form_mode->setFormClass('edit', 'Drupal\field_ui_ajax\Form\EntityDisplayModeAjaxEditForm');
  $form_mode->setFormClass('delete', 'Drupal\field_ui_ajax\Form\EntityDisplayModeAjaxDeleteForm');

  $view_mode = $entity_types['entity_view_mode'];
  $view_mode->setListBuilderClass('Drupal\field_ui_ajax\EntityDisplayModeAjaxListBuilder');
  $view_mode->setFormClass('add', 'Drupal\field_ui_ajax\Form\EntityDisplayModeAjaxAddForm');
  $view_mode->setFormClass('edit', 'Drupal\field_ui_ajax\Form\EntityDisplayModeAjaxEditForm');
  $view_mode->setFormClass('delete', 'Drupal\field_ui_ajax\Form\EntityDisplayModeAjaxDeleteForm');
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function field_ui_ajax_menu_local_tasks_alter(&$data, $route_name) {
  if (strpos($route_name, 'entity.entity_view_display') !== FALSE
    || strpos($route_name, 'entity.entity_form_display') !== FALSE) {
    $type = str_replace(['entity.entity_', '_display', '.view_mode', '.default'], '', $route_name);
    $type = str_replace(['.', '_'], '-', $type);
    if (isset($data['tabs']) && isset($data['tabs'][1])) {
      foreach ($data['tabs'][1] as $route => $tab) {
        $parameters = $tab['#link']['url']->getRouteParameters();
        $display = 'default';
        if (isset($parameters['view_mode_name'])) {
          $display = $parameters['view_mode_name'];
        }
        elseif (isset($parameters['form_mode_name'])) {
          $display = $parameters['form_mode_name'];
        }
        //if ($tab['#active']) {
        //  $data['tabs'][1][$route]['#link']['localized_options']['attributes']['class'][] = 'js-field-ui-toggle';
        //}
        //else {
          $data['tabs'][1][$route]['#link']['localized_options']['attributes']['class'][] = 'use-ajax';
          //$data['tabs'][1][$route]['#link']['localized_options']['attributes']['class'][] = 'use-ajax-once';
        //}
        $data['tabs'][1][$route]['#link']['localized_options']['attributes']['class'][] = 'js-field-ui-tabs';
        //$data['tabs'][1][$route]['#link']['localized_options']['attributes']['data-field-ui-show'] = '.js-' . $type . '-' . $display;
        //$data['tabs'][1][$route]['#link']['localized_options']['attributes']['data-field-ui-hide'] = '.js-manage-display-content';
      }
    }
  }
}

/**
 * Implements hook_preprocess_menu_local_action().
 */
function field_ui_ajax_preprocess_menu_local_action(&$variables) {
  $url = $variables['element']['#link']['url'];
  $route_name = $url->getRouteName();
  if (strpos($route_name, 'field_ui.field_storage_config_add_') === 0) {
    $variables['link']['#options']['attributes']['class'][] = 'use-ajax';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function field_ui_ajax_form_entity_view_display_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
  $form['#attached']['library'][] = 'field_ui_ajax/field_ui_ajax';
  $form['actions']['submit']['#ajax'] = [
    'callback' => '::entityViewDisplayAjaxSubmit',
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function field_ui_ajax_form_entity_form_display_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
  $form['#attached']['library'][] = 'field_ui_ajax/field_ui_ajax';
  $form['actions']['submit']['#ajax'] = [
    'callback' => '::entityFormDisplayAjaxSubmit',
  ];
}

/**
 * Implements hook_entity_operation_alter().
 */
function field_ui_ajax_entity_operation_alter(array &$operations, \Drupal\Core\Entity\EntityInterface $entity) {
  $entity_type_id = $entity->getEntityTypeId();
  if ($entity_type_id == 'entity_view_mode' || $entity_type_id == 'entity_form_mode') {
    $selector = 'js-' . str_replace(['.', '_'], '-', $entity->id());
    if (isset($operations['edit'])) {
      $operations['edit']['attributes']['class'][] = 'use-ajax';
      $operations['edit']['attributes']['class'][] = 'use-ajax-once';
      $operations['edit']['attributes']['data-field-ui-hide'][] = '.' . $selector;
      $operations['edit']['attributes']['data-field-ui-show'][] = '.' . $selector . '-edit-form';
    }
    if (isset($operations['delete'])) {
      $operations['delete']['attributes']['class'][] = 'use-ajax';
      $operations['delete']['attributes']['class'][] = 'use-ajax-once';
      $operations['delete']['attributes']['data-field-ui-hide'][] = '.' . $selector;
      $operations['delete']['attributes']['data-field-ui-show'][] = '.' . $selector . '-delete-form';
    }
  }
}

/**
 * Implements hook_pre_render_HOOK() for menu-local-tasks templates.
 *
 * Use preprocess hook to set classes to diferentiate between the primary and
 * secondary tasks.
 */
function field_ui_ajax_preprocess_block(&$variables) {
  if ($variables['configuration']['id'] == 'local_tasks_block' && $variables['configuration']['secondary']) {
    $variables['attributes']['class'][] = 'js-secondary-local-tasks';
  }
}

/**
 * Implements hook_preprocess_menu_local_tasks().
 */
function field_ui_ajax_preprocess_menu_local_tasks(&$variables) {
  /*if (empty($variables['secondary'])) {
    // Usually tabs are not displayed if there is a single one. But we need it
    // on the manage displays/form displays so AJAX commands can always have a
    // container for them on the page.
    $route_name = \Drupal::routeMatch()->getRouteName();
    if (strpos($route_name, 'entity.entity_view_display') !== FALSE
      || strpos($route_name, 'entity.entity_form_display') !== FALSE) {
      $manager = \Drupal::service('plugin.manager.menu.local_task');
      $links = $manager->getLocalTasks($route_name, 1);
      $tabs = count(Element::getVisibleChildren($links['tabs'])) > 0 ? $links['tabs'] : [];
      $variables['secondary'] = $tabs;
    }
  }
  dpm($variables);*/
}

/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 */
function field_ui_ajax_block_view_local_tasks_block_alter(array &$build, \Drupal\Core\Block\BlockPluginInterface $block) {
  if ($build['#configuration']['secondary']) {
    // Usually tabs are not displayed if there is a single one. But we need it
    // on the manage displays/form displays so AJAX commands can always have a
    // container for them on the page.
    $route_name = \Drupal::routeMatch()->getRouteName();
    if (strpos($route_name, 'entity.entity_view_display') !== FALSE
      || strpos($route_name, 'entity.entity_form_display') !== FALSE) {
      foreach ($build['#pre_render'] as $index => $function) {
        if ($function == 'Drupal\block\BlockViewBuilder::preRender') {
          // Normaly we should add this after the existing preRender functions.
          // But the way the original preRender works is that in case of empty
          // content it returns an array devoid of any information beside the
          // cache and an empty #markup.
          $build['#pre_render'][$index] = 'Drupal\field_ui_ajax\BlockViewBuilder::preRender';
        }
      }
    }
  }
}
