(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.razoreye = {
    attach: function(context, settings) {

        function moveItem() {
            $(".slick__slider").show();
        }

        setInterval(moveItem,1000);
    }
  };

})(jQuery, Drupal);
